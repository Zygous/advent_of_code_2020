use lazy_static::lazy_static;
use regex::Regex;

// All fields are optional so that we can build a Passport piecemeal
struct Passport {
    byr: Option<u16>,
    iyr: Option<u16>,
    eyr: Option<u16>,
    hgt: Option<String>,
    hcl: Option<String>,
    ecl: Option<String>,
    pid: Option<String>,
    cid: Option<u16>,
    is_weakly_valid: Option<bool>,
    is_strongly_valid: Option<bool>,
}

fn is_valid_byr(byr: Option<u16>) -> bool {
    match byr {
        None => false,
        Some(yr) => yr >= 1920 && yr <= 2002
    }
}

fn is_valid_iyr(iyr: Option<u16>) -> bool {
    match iyr {
        None => false,
        Some(yr) => yr >= 2010 && yr <= 2020
    }
}

fn is_valid_eyr(eyr: Option<u16>) -> bool {
    match eyr {
        None => false,
        Some(yr) => yr >= 2020 && yr <= 2030
    }
}

fn is_valid_hgt(hgt: Option<String>) -> bool {
    lazy_static! {
        static ref HGT_RE: Regex = Regex::new(r"^(\d\d\d?)(cm|in)$").unwrap();
    }

    match hgt {
        None => false,
        Some(hgt) => {
            match HGT_RE.captures(&hgt) {
                None => false,
                Some(caps) => {
                    let height: u16 = caps[1].parse().unwrap();
                    if &caps[2] == "cm" {
                        height >= 150 && height <= 193
                    } else {
                        height >= 59 && height <= 76
                    }
                }
            }
        }
    }
}

fn is_valid_hcl(hcl: Option<String>) -> bool {
    lazy_static! {
        static ref HCL_RE: Regex = Regex::new(r"^#[0-9a-f]{6}").unwrap();
    }

    match hcl {
        None => false,
        Some(colour) => HCL_RE.is_match(&colour)
    }
}

fn is_valid_ecl(ecl: Option<String>) -> bool {
    match ecl {
        None => false,
        Some(colour) => match &colour[..] {
            "amb" => true,
            "blu" => true,
            "brn" => true,
            "gry" => true,
            "grn" => true,
            "hzl" => true,
            "oth" => true,
            _ => false
        }
    }
}

fn is_valid_pid(pid: Option<String>) -> bool {
    lazy_static! {
        static ref PID_RE: Regex = Regex::new(r"^\d{9}$").unwrap();
    }

    match pid {
        None => false,
        Some(id) => PID_RE.is_match(&id)
    }
}

fn validate(passport: Passport) -> Passport {
    let weakly_valid = passport.byr.is_some() &&
        passport.iyr.is_some() &&
        passport.eyr.is_some() &&
        passport.hgt.is_some() &&
        passport.hcl.is_some() &&
        passport.ecl.is_some() &&
        passport.pid.is_some();

    let stongly_valid = weakly_valid &&
        is_valid_byr(passport.byr.clone()) &&
        is_valid_iyr(passport.iyr.clone()) &&
        is_valid_eyr(passport.eyr.clone()) &&
        is_valid_hgt(passport.hgt.clone()) &&
        is_valid_hcl(passport.hcl.clone()) &&
        is_valid_ecl(passport.ecl.clone()) &&
        is_valid_pid(passport.pid.clone());

    Passport {
        is_weakly_valid: Some(weakly_valid),
        is_strongly_valid: Some(stongly_valid),
        ..passport
    }
}

pub fn solve(input: String) {
    // Records are separated by empty lines, but without knowing whether the
    // input uses Windows line endings or not we don't know whether that means
    // records are separated by \n\n or \r\n\r\n. This is easily resolved by
    // stripping all the \r characters from the input.
    let mut transformed = input.trim().replace("\r", "");
    // If we now replace all "\n\n" with a sentinel value we can remove all the
    // other "\n" to transform the data to one complete record per line.
    transformed = transformed.replace("\n\n", "===");
    transformed = transformed.replace("\n", " ");
    transformed = transformed.replace("===", "\n");

    let records: Vec<Passport> = transformed.lines().map(|line| {
        let mut passport = Passport {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: None,
            is_weakly_valid: None,
            is_strongly_valid: None,
        };

        // Parse the line into key:value pairs
        for kv in line.split(" ") {
            let mut tokens = kv.trim().split(":");
            let val = tokens.clone().last().unwrap();
            let key = tokens.nth(0).unwrap();

            match key {
                "byr" => passport.byr = Some(val.parse().unwrap()),
                "iyr" => passport.iyr = Some(val.parse().unwrap()),
                "eyr" => passport.eyr = Some(val.parse().unwrap()),
                "hgt" => passport.hgt = Some(val.to_owned()),
                "hcl" => passport.hcl = Some(val.to_owned()),
                "ecl" => passport.ecl = Some(val.to_owned()),
                "pid" => passport.pid = Some(val.to_owned()),
                "cid" => passport.cid = Some(val.parse().unwrap()),
                _ => panic!("Unexpected field {}", key)
            }
        }

        validate(passport)
    }).collect();

    let weakly_valid_count = records.iter().filter(|p| {
        match p.is_weakly_valid {
            Some(valid) => valid,
            None => false
        }
    }).count();
    let strongly_valid_count = records.iter().filter(|p| {
        match p.is_strongly_valid {
            Some(valid) => valid,
            None => false
        }
    }).count();

    println!("4th (a): {}", weakly_valid_count);
    println!("4th (b): {}", strongly_valid_count);
}
