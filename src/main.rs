use std::path::Path;

use advent_of_code_2020::{
    day_01,
    day_02,
    day_03,
    day_04,
    day_05,
    day_06,
    day_07,
};

fn main() {
    let puzzle_id = std::env::args()
        .nth(1)
        .expect("You must specify a puzzle ID");
    let puzzle_file = format!("{}.txt", puzzle_id);

    let inputs_path = Path::new("puzzle-inputs");
    let input_path = inputs_path.join(puzzle_file);

    let input = std::fs::read_to_string(input_path).expect("Could not read puzzle input");

    match &puzzle_id[..] {
        "01" => day_01::solve(input),
        "02" => day_02::solve(input),
        "03" => day_03::solve(input),
        "04" => day_04::solve(input),
        "05" => day_05::solve(input),
        "06" => day_06::solve(input),
        "07" => day_07::solve(input),
        _ => {}
    };
}

