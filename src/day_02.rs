struct PasswordValidity {
    is_valid_for_1: bool,
    is_valid_for_2: bool,
}

pub fn solve(input: String) {
    let records = input.trim().split("\n").map(password_file_line_to_record);

    println!(
        "2nd (a): {}",
        records.clone().filter(|r| r.is_valid_for_1).count()
    );
    println!("2nd (b): {}", records.filter(|r| r.is_valid_for_2).count());
}

fn password_file_line_to_record(line: &str) -> PasswordValidity {
    let tokens = line.split(" ").collect::<Vec<&str>>();

    let t_min_max = tokens[0];
    let t_letter = tokens[1];
    let t_password = tokens[2];
    let password = t_password.to_owned();

    let min_max: Vec<usize> = t_min_max
        .split("-")
        .map(|n| n.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    let min = min_max[0];
    let max = min_max[1];

    let letter: char = t_letter.chars().nth(0).unwrap();

    let letter_count = password
        .chars()
        .scan(0, |n, c| {
            if c == letter {
                *n += 1;
            }

            Some(*n)
        })
        .last()
        .unwrap();
    let is_valid_for_1 = letter_count >= min && letter_count <= max;

    let left_char = password.chars().nth(min - 1).unwrap();
    let right_char = password.chars().nth(max - 1).unwrap();
    let is_valid_for_2 = (left_char == letter && right_char != letter)
        || (left_char != letter && right_char == letter);

    PasswordValidity { is_valid_for_1, is_valid_for_2 }
}

