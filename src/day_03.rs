pub fn solve(input: String) {
    let lines = input.lines();

    let mut xa: usize = 0;
    let mut xb: usize = 0;
    let mut xc: usize = 0;
    let mut xd: usize = 0;
    let mut xe: usize = 0;

    let step_a: usize = 3;
    let step_b: usize = 1;
    let step_c: usize = 5;
    let step_d: usize = 7;
    let step_e: usize = 1;

    let mut tree_count_a: u64 = 0;
    let mut tree_count_b: u64 = 0;
    let mut tree_count_c: u64 = 0;
    let mut tree_count_d: u64 = 0;
    let mut tree_count_e: u64 = 0;

    let mut focus_e = true;

    for line in lines {
        let square_a = line.chars().cycle().nth(xa).unwrap();
        let square_b = line.chars().cycle().nth(xb).unwrap();
        let square_c = line.chars().cycle().nth(xc).unwrap();
        let square_d = line.chars().cycle().nth(xd).unwrap();

        if square_a == '#' {
            tree_count_a += 1;
        }
        xa += step_a;

        if square_b == '#' {
            tree_count_b += 1;
        }
        xb += step_b;

        if square_c == '#' {
            tree_count_c += 1;
        }
        xc += step_c;
        
        if square_d == '#' {
            tree_count_d += 1;
        }
        xd += step_d;

        if focus_e {
            let square_e = line.chars().cycle().nth(xe).unwrap();
            if square_e == '#' {
                tree_count_e += 1;
            }
            xe += step_e;
        }

        focus_e = !focus_e;
    }

    println!("3rd (a): {}", tree_count_a);
    println!("3rd (b): {}", tree_count_a * tree_count_b * tree_count_c * tree_count_d * tree_count_e);
}
