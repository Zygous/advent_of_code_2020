use std::ops::Range;

fn binary_split(range: Range<u64>, low_or_high: char) -> Range<u64> {
    let new_size = (range.end - range.start) / 2;

    if new_size == 1 {
        // We must return a Range, but all but the first number will be
        // ignored, so it can just be n..n+1
        return match low_or_high {
            'F' | 'L' => (range.start)..(range.start + 1),
            'B' | 'R' => (range.end - 1)..(range.end),
            _ => panic!("Invalid low/high identifier {}", low_or_high)
        }
    }

    match low_or_high {
        'F' | 'L' => (range.start)..(range.start + new_size),
        'B' | 'R' => {
            let mut part = range.skip(new_size as usize);
            part.nth(0).unwrap()..(part.last().unwrap() + 1)
        },
        _ => panic!("Invalid low/high identifier {}", low_or_high)
    }
}

pub fn solve(input: String) {
    let lines = input.lines();

    let mut highest_id = 0;
    let valid_ids = lines.fold(Vec::new(), |mut ids, line| {
        let row_chars = line.chars().take(7);
        let col_chars = line.chars().skip(7);

        let row = row_chars.fold(0..128, binary_split).start;
        let col = col_chars.fold(0..8, binary_split).start;

        let id = (row * 8) + col;

        if id > highest_id {
            highest_id = id;
        }

        if row > 0 && row < 127 {
            ids.push(id);
        }

        ids
    });

    println!("5th (a): {}", highest_id);

    for id in valid_ids.iter() {
        let next = id + 1;
        if !valid_ids.contains(&next) {
            println!("5th (b): {}", id + 1);
            break;
        }
    }
}
