use itertools::iproduct;

pub fn solve(input: String) {
    let lines: Vec<u32> = input
        .trim()
        .split("\n")
        .map(|n| n.parse::<u32>().unwrap())
        .collect();

    for (a, b) in iproduct!(&lines, &lines) {
        if a + b == 2020 {
            println!("1st (a): {}", a * b);
            break;
        }
    }

    for (a, b, c) in iproduct!(&lines, &lines, &lines) {
        if a + b + c == 2020 {
            println!("1st (b): {}", a * b * c);
            break;
        }
    }
}


