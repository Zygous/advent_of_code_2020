use std::collections::{HashMap, HashSet};

use lazy_static::lazy_static;
use regex::Regex;

struct Bag {
    colour: String,
    contents: HashMap<String, u8>,
    can_be_in: HashSet<String>
}

pub fn solve(input: String) {
    lazy_static! {
        static ref CONTAINER_RE: Regex = Regex::new(r"^(.+) bags contain (.+).$").unwrap();
        static ref CONTENTS_RE: Regex = Regex::new(r"^(\d+) (.+) bags?").unwrap();
    }

    // We'll collect all the bags in a hash-map, so we can quickly access them
    let mut bags: HashMap<String, Bag> = HashMap::new();

    for line in input.lines() {
        let captures = CONTAINER_RE.captures(line).unwrap();

        let container_colour = captures.get(1).map_or("", |m| m.as_str());

        let contents = captures.get(2).map_or("", |m| m.as_str()).split(", ");
        for contained in contents {
            if contained == "no other bags" {
                continue;
            }

            let caps = CONTENTS_RE.captures(contained).unwrap();
            let number: u8 = caps.get(1).map_or("0", |m| m.as_str()).parse().unwrap();
            let colour = caps.get(2).map_or("", |m| m.as_str());

            let container = bags.entry(container_colour.to_owned()).or_insert(Bag {
                colour: container_colour.to_owned(),
                contents: HashMap::new(),
                can_be_in: HashSet::new()
            });
            container.contents.insert(colour.to_owned(), number);

            {
                let bag = bags.entry(colour.to_owned()).or_insert(
                    Bag {
                        colour: colour.to_owned(),
                        contents: HashMap::new(),
                        can_be_in: HashSet::new()
                    },
                );
                bag.can_be_in.insert(container_colour.to_owned());
            }
        }
    }

    let mut outers: HashSet<String> = HashSet::new();
    for holder in bags[&"shiny gold".to_owned()].can_be_in.iter() {
        find_outers(&bags, &outers, holder);
    }
}

fn find_outers(bags: &HashMap<String, Bag>, outers: &HashSet<String>, bag_colour: &String) {
    let holders = bags[bag_colour].can_be_in;

    if holders.is_empty() {
        outers.insert(*bag_colour);
        return;
    }

    for holder in holders {
        find_outers(&bags, &outers, &holder);
    }
}
