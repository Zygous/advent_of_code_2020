use std::collections::HashSet;

struct Group {
    person_answers: Vec<HashSet<char>>,
    yes_questions: HashSet<char>,
}
impl Group {
    fn yes_intersection_count(&self) -> u64 {
        let intersection: HashSet<char> = self.person_answers.iter().fold(
            self.person_answers.first().unwrap().clone(),
            |acc, set| acc.intersection(&set).map(|c| c.to_owned()).collect()
        );

        intersection.len() as u64
    }
}

pub fn solve(input: String) {
    let mut groups: Vec<Group> = Vec::new();

    let mut any_yes_count: u64 = 0;
    let mut all_yes_count: u64 = 0;

    let mut group = Group {
        person_answers: Vec::new(),
        yes_questions: HashSet::new()
    };
    for mut line in input.lines() {
        line = line.trim();

        if line.len() < 1 {
            any_yes_count += group.yes_questions.len() as u64;
            all_yes_count += group.yes_intersection_count();

            groups.push(group);

            group = Group {
                person_answers: Vec::new(),
                yes_questions: HashSet::new()
            };
            continue;
        }

        for c in line.chars() {
            group.yes_questions.insert(c);
        }

        group.person_answers.push(line.chars().collect::<HashSet<char>>());
    }

    // Don't forget the last group
    any_yes_count += group.yes_questions.len() as u64;
    all_yes_count += group.yes_intersection_count();
    groups.push(group);

    println!("6 (a): {}", any_yes_count);
    println!("6 (b): {}", all_yes_count);
}

